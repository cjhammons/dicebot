const {Die} = require('../models/Die');

module.exports.roll = function(rollArgs) {

    console.log('Received roll', rollArgs);
  
    var rollTotal = 0;
    var numberBreakdown = [];
    var adding = false;
    var showNumberBreakdown = false;
    var dice = [];
  
  
    // function die(dieSize, numberRolled=1){
    //   for (var i = 0; i < numberRolled; i++) {
    //     console.log('rolling d', dieSize);
    //     var dieRoll = Math.floor(Math.random() * (dieSize)) + 1;
  
    //     console.log('Rolled', dieRoll);
  
    //     if (rollArgs.length === 1 && dieSize === 20) {
    //       if (dieRoll === 20) {
    //         console.log('Natural 20');          
    //         natural20 = true;
    //       } else if (dieRoll === 1) {
    //         console.log('Natural 1');          
    //         natural1 = true;
    //       }
    //     }
        
    //     rollTotal = rollTotal + dieRoll;
    //     numberBreakdown.push(dieRoll);
    //     adding = false;
    //   }
    // }
  
    for (var i = 0; i < rollArgs.length; i++) {
      var arg = rollArgs[i];
  
      if (arg.charAt(0) === 'd') {
        var diceSize = parseInt(arg.slice(1));
        var die = new Die(diceSize);
        rollTotal = rollTotal + die.newRoll;
        dice.push(die);
        numberBreakdown.push(die.roll);

        console.log('Just rolled this die:', die);
        
      } else if (arg.includes('d')) {
        var dieArgs = arg.split('d');
        console.log('Split', arg, 'into', dieArgs);
        
        var numberOfDie = dieArgs[0];
        var diceSize = dieArgs[1];
        for (var j = 0; j < numberOfDie; j++){
          var die = new Die(diceSize);
          rolltotal = rolltotal + die.newRoll;
          numberBreakdown.push(die.roll);
          dice.push(die)
        }
        showNumberBreakdown = true;
      } else if (arg === '+') {
        adding = true;
      } else if (adding) {
        rollTotal = rollTotal + parseInt(arg);
        numberBreakdown.push(parseInt(arg));
        adding = false;
      }
  
    }

    if (dice.length === 1) {
      var die = dice[0];
      if (die.natural1) return 'Natural 1 LOL';
      if (die.natural20) return 'NATURAL 20 BABYYYYYYY';
    }

    if (numberBreakdown.length > 1) {
      return rollTotal + ' ' + '(' + numberBreakdown.toString() + ')';
    }
  
    return rollTotal;
} 