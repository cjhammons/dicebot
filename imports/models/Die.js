
module.exports.Die = class {
  constructor(size) {
    this.size = size;
    this.roll = 0;
    if (size === 20) {
      this.natural20 = false;
      this.natrual1 = false;
    }
  }

  get newRoll(){
    console.log('Rolling d', this.size);
    this.roll =  Math.floor(Math.random() * (this.size)) + 1;
    if (this.size === 20) {
      if (this.roll === 20) this.natural20 = true;
      else if (this.roll === 1) this.natrual1 = true;
    }
    console.log('Rolled ', this.roll);
    return this.roll;
  }

}

