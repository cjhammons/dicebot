#!/bin/bash
#Gets a commit message then pushes to gitlab

read -p 'Commit Message: ' commitMessage

git add .
git commit -m "$commitMessage"
echo Pushing to GitLab
git push origin master
