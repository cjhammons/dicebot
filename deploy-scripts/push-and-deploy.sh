#!/bin/bash
#Gets a commit message then pushes to gitlab and heroku

read -p 'Commit Message: ' commitMessage

git add .
git commit -m "$commitMessage"
echo Pushing to GitLab
git push origin master
echo Deploying to Heroku
git push heroku master
