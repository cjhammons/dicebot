const Discord = require('discord.js');
const config = require('./config.json');
const {roll} = require('./imports/command-functions/roll');


function help(){
  return 'Type !roll followed by the number and type of dice to be rolled. For example: 2d20 will roll 2 20 sided die.\n\nYou can add multiple die rolls together or add modifiers by typing a plus.\nExample: 2d12 + 4 will doll 2 12-sided die and add a 4 to the result.\n\nIf more than 1 die is rolled or a modifier is used the bot will provide a breakdown of the different rolls that will take the form of (roll1,roll2,roll3) \n\nYou can get real weird with it if you want. 4d20 + 3d12 + d800 + 70 is a valid input :P';
}

const bot = new Discord.Client();
bot.login(config.token);

bot.on('message', message => {
  if(message.author.bot) return;
  if(message.content.indexOf(config.prefix) !== 0) return;

  const args = message.content.slice(config.prefix.length).trim().split(/ +/g);
  console.log('Args', args);
  const command = args.shift().toLowerCase();

  console.log('Received command', command);

  function sendMessage(name) {
    var messagePayload = '';
    switch (command) {
      case 'ping':
        messagePayload = 'pong!';
        break;
      case 'roll':
        messagePayload = name + ' rolled '+ roll(args.slice(0));
        break;
      case 'help':
        messagePayload = help();
        break;
      default:
        messagePayload = command + ' is not a valid command';
    }
    console.log('Sending message:', messagePayload);
    message.channel.send(messagePayload);
  }

  if (message.guild) {
    message.guild.fetchMember(message.author).then(member =>  {
      sendMessage(member.nickname);
    });
  } else {
    sendMessage(message.author.username);
  }

  
});
